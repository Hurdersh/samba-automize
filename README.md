# samba-automize.sh

This tool was created for school, but it is also usefull. This script makes you able to automize mounting shared directory with literal name of a share, also it will check slashes and you don't will have problems with slashes
actually this tool works only for DEBIAN based distriution like debian,ubuntu,linux mint etc

## usage
to use this tool you need only to execute them :)
### clone the repo with
`$ git clone https://gitlab.com/Hurdersh/samba-automize.git`
### enter to the directory of project 
`$ cd samba-automize`
### give permission to execute the script
`$ chmod +x samba-automize.sh`
### execute the script as ROOT
`# bash samba-automize.sh <device name> <directory to mount>`

ex: sudo bash samba-automize.sh  //rasp  /home/$USER/testdir

you can use all types of slashes to indicate device name, but there is DEVICE NAME.
not SHARE name.

