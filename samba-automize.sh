#!/bin/bash
#giordano colli 5 ainf giordanocolli@giordanocolli.ovh
print_help () {
	printf "usage: samba_automize <servername> <localdir> \n"
}
if [ "$EUID" -ne 0 ]
then
	printf "%s\n" "please run as root"
	exit 1
fi
if ! ping -c 1 www.google.it &> /dev/null
then
	printf "connect to the internet\n"
	exit 2
fi
if [[ "$1" == "" ]] || [[ "$2" == "" ]] 
then
	print_help 
	exit 3
fi

server_name="${1//\/}"
server_name="${server_name//\\}"
mount_dir="$2" 
if [ -d $mount_dir ]
then
	if ! [ "$(ls "$mount_dir" -h|wc -l)" -eq 0 ] || ! [ -w "$mount_dir" ]
	then
		printf "select valid mount_directory\n"
		exit 4
	fi
else
	printf "select valid mount_directory\n"
	exit 4

fi	


printf "checking dependencies "
if ! dpkg -s samba &> /dev/null || ! dpkg -s samba-common &> /dev/null || ! dpkg -s cifs-utils &> /dev/null || ! dpkg -s smbclient &> /dev/null
then
	points="."
	ended=false
	#command will executed 
	apt update -y &> /dev/null && apt install samba samba-common cifs-utils smbclient -y &> /dev/null &
	aptprocess=$!
	while [[ "$ended" == "false" ]]
	do
		printf $points
		sleep 1
		if ! pgrep -x apt &> /dev/null
		then
			ended=true
		fi
	done
	wait $aptprocess
	if [ $? -eq 0 ]
	then
		printf " OK\n"
	else
		printf " ERROR OCCURED\n"
	fi
else
	printf " OK\n"
fi

#check of existance of shared folder
if nmblookup "$server_name"
then
	server_ip=$(nmblookup "$server_name"|cut -d " " -f 1)
	choice=""
	read -p "do you want to acced with user and pass[Y/n]" choice
	if [[ "$choice" == [Yy] ]]
	then
		user=""
		password=""
		read -p "username: " user
		read -p "password: " password
	fi
	directory=""
	read -p "directory name: " directory
	directory="${directory//\\//}"
	directory=$(echo "$directory"| tr -s '/')
	if ! [[ "$directory" == /* ]]
	then
		directory="/$directory"
	fi
	#check if exists samba share
	if smbclient -L "$server_ip" 2> /dev/null<<< "\r" | grep -i "${directory:1:${#directory}}" &> /dev/null
	then
		if ! [[ "$user" == "" ]]
		then
			#you can functionify this
			printf "trying to mount\n"
			mount -t cifs -o user="$user",rw,users,password="$password" //"$server_ip""$directory" "$mount_dir" &> /dev/null &
			mount_pid="$!"
			while pgrep -x mount &> /dev/null
			do
				printf "."
			done
			wait "$mount_pid"
			if [ $? -eq 0 ]
			then
				printf "correctly mounted\n"
			else
				printf "an error occured\n"
				exit 7
			fi
		else
			printf "trying to mount\n"
			mount -t cifs -o rw,users,password="" //"$server_ip""$directory" "$mount_dir" &> /dev/null &
			mount_pid="$!"
			while pgrep -x mount &> /dev/null
			do
				printf "."
			done
			wait "$mount_pid"
			if [ $? -eq 0 ]
			then
				printf "correctly mounted\n"
			else
				printf "an error occured\n"
				exit 7
			fi
		fi
	else
		printf "shared folder with this name not exist\n"
		exit 6
	fi
	#end of all
else
	printf "server name not found"
	exit 5
fi
